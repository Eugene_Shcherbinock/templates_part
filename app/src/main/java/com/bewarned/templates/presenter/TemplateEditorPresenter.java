package com.bewarned.templates.presenter;

import com.bewarned.templates.App;
import com.bewarned.templates.R;
import com.bewarned.templates.contract.TemplateEditorContract;
import com.bewarned.templates.data.TemplatesRepository;
import com.bewarned.templates.model.Template;

public class TemplateEditorPresenter implements TemplateEditorContract.Presenter {

    private TemplateEditorContract.View mView;
    private TemplatesRepository mRepository;

    public TemplateEditorPresenter(TemplateEditorContract.View view) {
        attachView(view);
    }

    @Override
    public void editTemplate(Template template) {
        if (mView != null) {
            if (!isTemplateCorrect(template)) {
                mView.showError(mView.getContext().getString(R.string.str_input_template_error));
                return;
            }
            mRepository.update(template.getId(), template);
        }
    }

    @Override
    public void addTemplate(Template template) {
        if (mView != null) {
            if (!isTemplateCorrect(template)) {
                mView.showError(mView.getContext().getString(R.string.str_input_template_error));
                return;
            }
            mRepository.create(template);
        }
    }

    @Override
    public void attachView(TemplateEditorContract.View view) {
        mView = view;
        mRepository = App.getTemplatesRepository();
    }

    @Override
    public void detachView() {
        mView = null;
        mRepository = null;
    }

    private boolean isTemplateCorrect(Template template) {
        return template.getMessage().length() > 0;
    }
}
