package com.bewarned.templates.presenter;

import com.bewarned.templates.App;
import com.bewarned.templates.contract.TemplatesContract;
import com.bewarned.templates.data.TemplatesRepository;
import com.bewarned.templates.model.Template;

import java.util.List;

public class TemplatesPresenter implements TemplatesContract.Presenter {

    private TemplatesRepository mRepository;
    private TemplatesContract.View mView;

    public TemplatesPresenter(TemplatesContract.View view) {
        attachView(view);
    }

    @Override
    public void getTemplatesList() {
        if (mView != null) {
            List<Template> templates = mRepository.getAll();
            mView.showTemplates(templates);
        }
    }

    @Override
    public void selectTemplate(Template template) {
        if (mView != null) {
            mView.editTemplate(template);
        }
    }

    @Override
    public void addTemplate() {
        if (mView != null) {
            mView.editTemplate(null);
        }
    }

    @Override
    public void attachView(TemplatesContract.View view) {
        mView = view;
        mRepository = App.getTemplatesRepository();
    }

    @Override
    public void detachView() {
        mView = null;
        mRepository = null;
    }
}
