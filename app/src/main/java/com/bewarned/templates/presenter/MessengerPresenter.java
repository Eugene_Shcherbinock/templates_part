package com.bewarned.templates.presenter;

import com.bewarned.templates.App;
import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.contract.MessengerContract;
import com.bewarned.templates.data.MessagesRepository;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;

import java.util.List;

public class MessengerPresenter implements MessengerContract.Presenter {

    private MessengerContract.View mView;

    public MessengerPresenter(MessengerContract.View view) {
        attachView(view);
    }

    @Override
    public void getAllMessages(Dialog dialog) {
        if (mView != null) {
//            List<Message> messages = dialog.getMessages();
            MessagesRepository messagesRepository = new MessagesRepository(mView.getContext(), dialog);
            List <Message> messages = messagesRepository.getAll();
            mView.showAllMessages(messages);
        }
    }

    @Override
    public int getMessagesCount(Dialog dialog) {
        MessagesRepository messagesRepository = new MessagesRepository(mView.getContext(), dialog);
        return messagesRepository.getCount();
    }

    @Override
    public void getMessagesInLimit(Dialog dialog, int offset, int count) {
        if (mView != null) {
            MessagesRepository messagesRepository = new MessagesRepository(mView.getContext(), dialog);
//            List <Message> messages = messagesRepository.getInLimits(offset, count);
            mView.showNewMessages(messagesRepository.getInLimits(offset, count), true);
        }
    }

    @Override
    public void getResponders(Dialog dialog) {
        if (mView != null) {
//            List<Responder> responders = dialog.getResponders();
            mView.showResponders(dialog.getResponders());
        }
    }

    @Override
    public void sendMessage(Dialog dialog, Message message) {
        if (mView != null) {
            if (message.getMessage().isEmpty()) {
                mView.showError(mView.getContext().getString(R.string.str_incorrect_message_error));
            } else {
                dialog.setLastMessage(message.getMessage());
                dialog.setLastMessageDate(message.getDate());

                MessagesRepository messagesRepository = new MessagesRepository(mView.getContext(), dialog);
                messagesRepository.create(message);

                App.getDialogsRepository().update(dialog.getId(), dialog);

                List<Message> messages = messagesRepository.getInLimits(0, Constants.MESSAGES_LOAD_LIMIT);
                mView.showNewMessages(messages, false);
            }
        }
    }

    @Override
    public void attachView(MessengerContract.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
