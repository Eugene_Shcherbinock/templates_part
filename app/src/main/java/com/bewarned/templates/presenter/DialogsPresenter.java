package com.bewarned.templates.presenter;

import com.bewarned.templates.App;
import com.bewarned.templates.contract.DialogsContract;
import com.bewarned.templates.data.DialogsRepository;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.util.DateUtils;

import java.util.Date;
import java.util.List;

public class DialogsPresenter implements DialogsContract.Presenter {

    private DialogsRepository mRepository;
    private DialogsContract.View mView;

    public DialogsPresenter(DialogsContract.View view) {
        attachView(view);
    }

    @Override
    public void getDialogs() {
        if (mView != null) {
            List<Dialog> dialogs = mRepository.getAll();
            mView.showDialogs(dialogs);
        }
    }

    @Override
    public void selectDialog(Dialog dialog) {
        if (mView != null) {
            mView.showDialog(dialog);
        }
    }

    @Override
    public void createDialog() {
        Dialog newDialog = new Dialog();
        newDialog.setRespondersCount(1);
        newDialog.setLastMessageDate(DateUtils.formatDate(new Date()));
        mRepository.create(newDialog);
        if (mView != null) {
            mView.showDialog(newDialog);
        }
    }

    @Override
    public void attachView(DialogsContract.View view) {
        mView = view;
        mRepository = App.getDialogsRepository();
    }

    @Override
    public void detachView() {
        mView = null;
        mRepository = null;
    }
}
