package com.bewarned.templates.presenter;

import com.bewarned.templates.App;
import com.bewarned.templates.contract.StartContract;
import com.bewarned.templates.data.MessagesRepository;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.model.Responder;
import com.bewarned.templates.util.DateUtils;

import java.util.Date;

public class StartPresenter implements StartContract.Presenter {

    private StartContract.View mView;

    public StartPresenter(StartContract.View view) {
        attachView(view);
    }

    @Override
    public void startDictation() {
        if (mView != null) {
            App.sRecognizerStarted = true;
            mView.showCounter();
        }
    }

    @Override
    public void stopDictation() {
        App.sRecognizerStarted = false;
        if (mView != null) {
            mView.hideCounter();
        }
    }

    @Override
    public void createEmptyDialog() {
        Dialog newDialog = new Dialog();
        newDialog.setRespondersCount(1);
        newDialog.setLastMessageDate(DateUtils.formatDate(new Date()));
        App.getDialogsRepository().create(newDialog);

        if (mView != null) {
            mView.showMessenger(newDialog, true);
        }
    }

    @Override
    public void createDialogWithMessage(String message) {
        Dialog newDialog = new Dialog();
        newDialog.setRespondersCount(1);
        newDialog.setLastMessage(message);
        newDialog.setLastMessageDate(DateUtils.formatDate(new Date()));
        App.getDialogsRepository().create(newDialog);

        if (mView != null) {
            Responder meResponder = newDialog.getResponders().get(1);

            MessagesRepository messagesRepository = new MessagesRepository(mView.getContext(), newDialog);
            messagesRepository.create(new Message(meResponder, message, DateUtils.formatDate(new Date())));

            mView.showMessenger(newDialog, false);
        }
    }

    @Override
    public void attachView(StartContract.View view) {
        mView = view;
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
