package com.bewarned.templates.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bewarned.templates.R;

public class MessageViewHolder extends RecyclerView.ViewHolder {

    protected ImageView mImageViewProfile;

    protected TextView mTextViewMessage;
    protected TextView mTextViewMessageDate;

    public MessageViewHolder(View itemView) {
        super(itemView);

        mImageViewProfile = (ImageView) itemView.findViewById(R.id.image_view_dialog_profile);
        mTextViewMessage = (TextView) itemView.findViewById(R.id.text_view_dialog_message);
        mTextViewMessageDate = (TextView) itemView.findViewById(R.id.text_view_dialog_message_date);
    }

    public ImageView getProfileImageView() {
        return mImageViewProfile;
    }

    public TextView getMessageView() {
        return mTextViewMessage;
    }

    public TextView getMessageDateView() {
        return mTextViewMessageDate;
    }
}
