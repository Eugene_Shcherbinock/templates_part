package com.bewarned.templates.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bewarned.templates.R;
import com.bewarned.templates.interfaces.OnResponderClickListener;
import com.bewarned.templates.model.Responder;

public class ResponderViewHolder extends RecyclerView.ViewHolder {

    private View mResponderItemView;
    private View mViewActiveIndicator;

    private ImageView mImageViewResponderIcon;
    private TextView mTextViewResponderName;

    public ResponderViewHolder(View view) {
        super(view);

        mResponderItemView = view;
        mViewActiveIndicator = view.findViewById(R.id.view_active_indicator);
        mImageViewResponderIcon = (ImageView) view.findViewById(R.id.image_view_responder_icon);
        mTextViewResponderName = (TextView) view.findViewById(R.id.text_view_responder_name);
    }

    public View getResponderItemView() {
        return mResponderItemView;
    }

    public View getActiveIndicatorView() {
        return mViewActiveIndicator;
    }

    public ImageView getResponderIconView() {
        return mImageViewResponderIcon;
    }

    public TextView getResponderNameView() {
        return mTextViewResponderName;
    }

    public void setItemClickListener(final Responder responder, final OnResponderClickListener listener) {
        mResponderItemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onResponderClick(responder);
            }
        });
    }

}
