package com.bewarned.templates.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bewarned.templates.App;
import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.contract.MessengerContract;
import com.bewarned.templates.interfaces.OnResponderClickListener;
import com.bewarned.templates.interfaces.OnUpdateTitleListener;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.model.Responder;
import com.bewarned.templates.presenter.MessengerPresenter;
import com.bewarned.templates.ui.adapter.MessagesRecyclerAdapter;
import com.bewarned.templates.ui.adapter.RespondersRecyclerAdapter;
import com.bewarned.templates.util.ActivityUtils;
import com.bewarned.templates.util.DateUtils;
import com.bewarned.templates.util.KeyboardUtils;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessengerActivity extends AppCompatActivity implements MessengerContract.View,
        OnUpdateTitleListener {

    private Toolbar mToolbar;
    private EditText mEditTextDialogTitle;
    private View.OnClickListener mToolbarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mEditTextDialogTitle.setText(mToolbar.getTitle());
            mEditTextDialogTitle.setVisibility(View.VISIBLE);
            mEditTextDialogTitle.requestFocus();
            KeyboardUtils.showKeyboard(MessengerActivity.this, mEditTextDialogTitle);
            mToolbar.setTitle("");
        }
    };
    private View.OnFocusChangeListener mEditTextFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                switch (v.getId()) {
                    case R.id.toolbar_edit_text:
                        updateDialogTitle();
                        break;
                    case R.id.edit_text_message:
                        KeyboardUtils.hideKeyboard(MessengerActivity.this, mEditTextMessage);
                        break;
                }
            }
        }
    };

    private Dialog mDialog;
    private MessengerContract.Presenter mPresenter;

    private RecyclerView mRecyclerViewMessages;
    private MessagesRecyclerAdapter mMessagesRecyclerAdapter;

    private RecyclerView mRecyclerViewResponders;
    private RespondersRecyclerAdapter mRespondersAdapter;
    private OnResponderClickListener mOnResponderClickListener = new OnResponderClickListener() {
        @Override
        public void onResponderClick(Responder responder) {
            mRespondersAdapter.selectItem(responder);
        }
    };

    private EditText mEditTextMessage;
    private FloatingActionButton mFloatButtonSendMessage;
    private boolean mIsShowKeyboard;

    private int mAllMessagesCount;
    private int mCurrentMessagesCount;

    private int mLastVisibleItem;
    private boolean mIsLoadingStopped;

    private RecyclerView.OnScrollListener mPaginationScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            int totalItem = layoutManager.getItemCount();
            mLastVisibleItem = layoutManager.findLastVisibleItemPosition();

            if (!mIsLoadingStopped && mLastVisibleItem == totalItem - 1) {
                mIsLoadingStopped = true;
                if (mCurrentMessagesCount != mAllMessagesCount) {
                    mPresenter.getMessagesInLimit(mDialog, mCurrentMessagesCount, Constants.MESSAGES_LOAD_LIMIT);
                    mIsLoadingStopped = false;
                }
            }
        }
    };

    private View.OnClickListener mOnSendMessageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Message newMessage = new Message(
                    mRespondersAdapter.getSelectedResponder(),
                    mEditTextMessage.getText().toString(),
                    DateUtils.formatDate(new Date())
            );
            mPresenter.sendMessage(mDialog, newMessage);
            mIsLoadingStopped = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);

        receiveArguments();

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mEditTextDialogTitle = (EditText) findViewById(R.id.toolbar_edit_text);

        mToolbar.setOnClickListener(mToolbarClickListener);
        mEditTextDialogTitle.setOnFocusChangeListener(mEditTextFocusChangeListener);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initializeMessagesList();
        initializeMessageBar();

        showKeyboard();

        mPresenter = new MessengerPresenter(this);

        mAllMessagesCount = mPresenter.getMessagesCount(mDialog);
        mPresenter.getMessagesInLimit(mDialog, mCurrentMessagesCount, Constants.MESSAGES_LOAD_LIMIT);
        mPresenter.getResponders(mDialog);
    }

    @Override
    public void onBackPressed() {
        if (mEditTextDialogTitle.getVisibility() == View.VISIBLE) {
            updateDialogTitle();
            return;
        }
        ActivityUtils.startActivity(this, MainActivity.class, new Bundle());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public Context getContext() {
        return App.getContext();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAllMessages(List<Message> messages) {
        mEditTextMessage.setText("");
        mMessagesRecyclerAdapter.addMessages(messages, false);
        showKeyboard();
    }

    @Override
    public void showNewMessages(List<Message> messages, boolean isPagination) {
        mEditTextMessage.setText("");

        mMessagesRecyclerAdapter.addMessages(messages, isPagination);
        if (isPagination) {
            mRecyclerViewMessages.smoothScrollToPosition(mLastVisibleItem);
            mCurrentMessagesCount += messages.size();
        } else {
            mAllMessagesCount = mPresenter.getMessagesCount(mDialog);
            mCurrentMessagesCount = messages.size();
        }
    }

    @Override
    public void showResponders(List<Responder> responders) {
        mRespondersAdapter = new RespondersRecyclerAdapter(this, responders, mOnResponderClickListener);
        mRecyclerViewResponders.setAdapter(mRespondersAdapter);

        onUpdateDefaultTitle(responders.size());
    }

    @Override
    @SuppressWarnings("ConstantConditions")
    public void onUpdateDefaultTitle(int respondersCount) {
        if ("".equals(mDialog.getTitle())) {
            getSupportActionBar().setTitle(mDialog.getRespondersCount() + "-persons Connect-dialog");
            return;
        }
        getSupportActionBar().setTitle(mDialog.getTitle());
    }

    private void updateDialogTitle() {
        mDialog.setTitle(mEditTextDialogTitle.getText().toString());
        App.getDialogsRepository().update(mDialog.getId(), mDialog);

        mToolbar.setTitle(mDialog.getTitle());
        mEditTextDialogTitle.setVisibility(View.GONE);
        KeyboardUtils.hideKeyboard(MessengerActivity.this, mEditTextDialogTitle);
    }

    private void receiveArguments() {
        Intent intentArguments = getIntent();
        Bundle arguments = intentArguments.getExtras();
        if (arguments != null) {
            long dialogId = arguments.getLong(Constants.DIALOG_ARGUMENT);
            mDialog = Dialog.load(Dialog.class, dialogId);
            mIsShowKeyboard = arguments.getBoolean(Constants.KEYBOARD_ARGUMENT);
        }
    }

    private void initializeMessagesList() {
        mRecyclerViewMessages = (RecyclerView) findViewById(R.id.recycler_view_messages);

        RecyclerView.LayoutManager messagesLayoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, true);

        mRecyclerViewMessages.setLayoutManager(messagesLayoutManager);
        mRecyclerViewMessages.addOnScrollListener(mPaginationScrollListener);
        mRecyclerViewMessages.setHasFixedSize(false);
        mMessagesRecyclerAdapter = new MessagesRecyclerAdapter(this, new ArrayList<Message>());
        final StickyRecyclerHeadersDecoration stickyHeaders =
                new StickyRecyclerHeadersDecoration(mMessagesRecyclerAdapter);

        mMessagesRecyclerAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                                                                 @Override
                                                                 public void onChanged() {
                                                                     super.onChanged();
                                                                     stickyHeaders.invalidateHeaders();
                                                                 }
                                                             });
        mRecyclerViewMessages.addItemDecoration(new StickyRecyclerHeadersDecoration(mMessagesRecyclerAdapter));
        mRecyclerViewMessages.setAdapter(mMessagesRecyclerAdapter);
        mRecyclerViewMessages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerViewMessages.requestFocus();
            }
        });
    }

    private void initializeMessageBar() {
        mRecyclerViewResponders = (RecyclerView) findViewById(R.id.recycler_view_responders);
        mEditTextMessage = (EditText) findViewById(R.id.edit_text_message);
        mFloatButtonSendMessage = (FloatingActionButton) findViewById(R.id.float_button_send_message);

        RecyclerView.LayoutManager respondersLayoutManager = new LinearLayoutManager(
                this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerViewResponders.setLayoutManager(respondersLayoutManager);
        mFloatButtonSendMessage.setOnClickListener(mOnSendMessageClickListener);
        mEditTextMessage.setOnFocusChangeListener(mEditTextFocusChangeListener);
    }

    private void showKeyboard() {
        if (mIsShowKeyboard) {
            mEditTextMessage.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mEditTextMessage.requestFocus();
                    KeyboardUtils.showKeyboard(getContext(), mEditTextMessage);
                }
            }, 50);
        }
    }
}
