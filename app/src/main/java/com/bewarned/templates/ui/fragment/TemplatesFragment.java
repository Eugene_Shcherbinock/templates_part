package com.bewarned.templates.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bewarned.templates.R;
import com.bewarned.templates.contract.TemplatesContract;
import com.bewarned.templates.interfaces.OnShowEditorListener;
import com.bewarned.templates.model.Template;
import com.bewarned.templates.presenter.TemplatesPresenter;
import com.bewarned.templates.ui.adapter.TemplatesListAdapter;
import com.bewarned.templates.ui.adapter.action.TemplatesActionMode;

import java.util.List;


public class TemplatesFragment extends Fragment implements TemplatesContract.View {

    public static final int POSITION = 2;

    private View mRootView;
    private ListView mListViewTemplates;
    private TemplatesListAdapter mTemplatesAdapter;

    private OnShowEditorListener mOnEditTemplate;
    private TemplatesContract.Presenter mPresenter;

    private FloatingActionButton mFloatButtonNewTemplate;

    private AdapterView.OnItemClickListener mOnTemplateClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mPresenter.selectTemplate(mTemplatesAdapter.getItem(position));
        }
    };
    private View.OnClickListener mOnAddTemplateClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPresenter.addTemplate();
        }
    };

    public TemplatesFragment() {
        // Required empty public constructor
    }

    public static TemplatesFragment newInstance() {
        return new TemplatesFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShowEditorListener) {
            mOnEditTemplate = (OnShowEditorListener) context;
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new TemplatesPresenter(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_templates, container, false);

        mListViewTemplates = (ListView) mRootView.findViewById(R.id.list_view_templates);
        mListViewTemplates.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListViewTemplates.setOnItemClickListener(mOnTemplateClick);
        mListViewTemplates.setMultiChoiceModeListener(new TemplatesActionMode(getContext(), mListViewTemplates));

        mFloatButtonNewTemplate = (FloatingActionButton) mRootView.findViewById(R.id.float_button_add_new_template);
        mFloatButtonNewTemplate.setOnClickListener(mOnAddTemplateClick);

        mPresenter.getTemplatesList();

        return mRootView;
    }

    @Override
    public void showTemplates(List<Template> templates) {
        mTemplatesAdapter = new TemplatesListAdapter(getContext(), templates);
        mListViewTemplates.setAdapter(mTemplatesAdapter);
    }

    @Override
    public void editTemplate(Template template) {
        if (mOnEditTemplate != null) {
            mOnEditTemplate.onShowEditor(template);
        }
    }
}
