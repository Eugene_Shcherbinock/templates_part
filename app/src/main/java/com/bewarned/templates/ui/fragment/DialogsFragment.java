package com.bewarned.templates.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bewarned.templates.R;
import com.bewarned.templates.contract.DialogsContract;
import com.bewarned.templates.interfaces.OnShowMessengerListener;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.presenter.DialogsPresenter;
import com.bewarned.templates.ui.adapter.DialogsListAdapter;
import com.bewarned.templates.ui.adapter.action.DialogsActionMode;

import java.util.List;

public class DialogsFragment extends Fragment implements DialogsContract.View {

    public static final int POSITION = 1;

    private View mRootView;
    private ListView mListViewDialogs;
    private DialogsListAdapter mDialogsAdapter;

    private FloatingActionButton mFloatButtonCreateDialog;

    private DialogsContract.Presenter mPresenter;

    private boolean mIsShowKeyboard;
    private OnShowMessengerListener mOnShowDialog;

    private AdapterView.OnItemClickListener mOnDialogClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mPresenter.selectDialog(mDialogsAdapter.getItem(position));
        }
    };
    private View.OnClickListener mOnCreateDialogClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mIsShowKeyboard = true;
            mPresenter.createDialog();
        }
    };

    public DialogsFragment() {
        // Required empty public constructor
    }

    public static DialogsFragment newInstance() {
        return new DialogsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShowMessengerListener) {
            mOnShowDialog = (OnShowMessengerListener) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new DialogsPresenter(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_dialogs, container, false);

        mIsShowKeyboard = false;

        mFloatButtonCreateDialog = (FloatingActionButton) mRootView.findViewById(R.id.float_button_add_new_dialog);
        mFloatButtonCreateDialog.setOnClickListener(mOnCreateDialogClickListener);

        mListViewDialogs = (ListView) mRootView.findViewById(R.id.list_view_dialogs);
        mListViewDialogs.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        mListViewDialogs.setOnItemClickListener(mOnDialogClick);
        mListViewDialogs.setMultiChoiceModeListener(new DialogsActionMode(getContext(), mListViewDialogs));

        mPresenter.getDialogs();

        return mRootView;
    }

    @Override
    public void showDialogs(List<Dialog> dialogs) {
        mDialogsAdapter = new DialogsListAdapter(getContext(), dialogs);
        mListViewDialogs.setAdapter(mDialogsAdapter);
    }

    @Override
    public void showDialog(Dialog dialog) {
        if (mOnShowDialog != null) {
            mOnShowDialog.onShowMessenger(dialog, mIsShowKeyboard);
        }
    }
}
