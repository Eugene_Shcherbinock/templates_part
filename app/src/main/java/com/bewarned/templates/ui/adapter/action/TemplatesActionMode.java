package com.bewarned.templates.ui.adapter.action;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;

import com.bewarned.templates.R;
import com.bewarned.templates.ui.adapter.TemplatesListAdapter;

public class TemplatesActionMode implements AbsListView.MultiChoiceModeListener {

    private Context mContext;
    private ListView mListViewTemplates;

    private int mSelectedCounter;

    public TemplatesActionMode(Context context, ListView listView) {
        mSelectedCounter = 0;
        mContext = context;
        mListViewTemplates = listView;
    }

    @Override
    public void onItemCheckedStateChanged(android.view.ActionMode mode, final int position, long id, boolean checked) {
        mSelectedCounter += checked ? 1 : -1;
        mode.setTitle(mContext.getString(R.string.str_action_mode_title) + "(" + mSelectedCounter + ")?");
        mode.invalidate();
    }

    @Override
    public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
        mode.getMenuInflater().inflate(R.menu.action_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.delete_item:
                ((TemplatesListAdapter) mListViewTemplates.getAdapter())
                        .deleteTemplates(mListViewTemplates.getCheckedItemPositions());

                Snackbar.make(mListViewTemplates,
                        mContext.getString(R.string.str_action_mode_notification),
                        Snackbar.LENGTH_SHORT).show();

                mode.finish();
                break;

            case R.id.close_item:
                mode.finish();
                break;
        }
        return true;
    }

    @Override
    public void onDestroyActionMode(android.view.ActionMode mode) {
        mSelectedCounter = 0;
    }
}
