package com.bewarned.templates.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bewarned.templates.R;

public class TemplateViewHolder extends RecyclerView.ViewHolder {

    private TextView mTextViewNotification;
    private TextView mTextViewMessage;
    private ToggleButton mToggleButton;

    public TemplateViewHolder(View view) {
        super(view);

        mTextViewNotification = (TextView) view.findViewById(R.id.text_view_notification);
        mTextViewMessage = (TextView) view.findViewById(R.id.text_view_message);
        mToggleButton = (ToggleButton) view.findViewById(R.id.toggle_button_speech);
    }

    public TextView getTextViewNotification() {
        return mTextViewNotification;
    }

    public TextView getTextViewMessage() {
        return mTextViewMessage;
    }

    public ToggleButton getToggleButton() {
        return mToggleButton;
    }

}
