package com.bewarned.templates.ui.adapter.holder;

import android.view.View;
import android.widget.TextView;

import com.bewarned.templates.R;

public class MessageDateDividerViewHolder extends MessageViewHolder {

    private TextView mTextViewDateDivider;

    public MessageDateDividerViewHolder(View itemView) {
        super(itemView);
        mTextViewDateDivider = (TextView) itemView.findViewById(R.id.text_view_messages_date);
    }

    public TextView getDateDividerView() {
        return mTextViewDateDivider;
    }
}
