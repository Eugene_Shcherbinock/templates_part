package com.bewarned.templates.ui.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.contract.MessengerContract;
import com.bewarned.templates.interfaces.OnResponderClickListener;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.model.Responder;
import com.bewarned.templates.presenter.MessengerPresenter;
import com.bewarned.templates.ui.adapter.MessagesRecyclerAdapter;
import com.bewarned.templates.ui.adapter.RespondersRecyclerAdapter;
import com.bewarned.templates.util.DateUtils;
import com.bewarned.templates.util.KeyboardUtils;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessengerFragment extends Fragment implements MessengerContract.View {

    private final int MESSAGES_LOAD_LIMIT = 5;

    private Dialog mDialog;
    private MessengerContract.Presenter mPresenter;

    private View mRootView;
    private RecyclerView mRecyclerViewMessages;
    private MessagesRecyclerAdapter mMessagesRecyclerAdapter;

    private RecyclerView mRecyclerViewResponders;
    private RespondersRecyclerAdapter mRespondersAdapter;
    private OnResponderClickListener mOnResponderClickListener = new OnResponderClickListener() {
        @Override
        public void onResponderClick(Responder responder) {
            mRespondersAdapter.selectItem(responder);
        }
    };

    private EditText mEditTextMessage;
    private FloatingActionButton mFloatButtonSendMessage;
    private boolean mIsShowKeyboard;

    private int mAllMessagesCount;
    private int mCurrentMessagesCount;

    private int mLastVisibleItem;
    private boolean mIsLoadingStopped;

    private RecyclerView.OnScrollListener mPaginationScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            int totalItem = layoutManager.getItemCount();
            mLastVisibleItem = layoutManager.findLastVisibleItemPosition();

            if (!mIsLoadingStopped && mLastVisibleItem == totalItem - 1) {
                mIsLoadingStopped = true;
                if (mCurrentMessagesCount != mAllMessagesCount) {
                    mPresenter.getMessagesInLimit(mDialog, mCurrentMessagesCount, MESSAGES_LOAD_LIMIT);
                    mIsLoadingStopped = false;
                }
            }
        }
    };

    private View.OnClickListener mOnSendMessageClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Message newMessage = new Message(
                    mRespondersAdapter.getSelectedResponder(),
                    mEditTextMessage.getText().toString(),
                    DateUtils.formatDate(new Date())
            );
            mPresenter.sendMessage(mDialog, newMessage);
            mIsLoadingStopped = false;
        }
    };

    public MessengerFragment() {
        // Required empty public constructor
    }

    public static MessengerFragment newInstance(@NonNull Dialog dialog, boolean showKeyboard) {
        MessengerFragment fragment = new MessengerFragment();

        Bundle args = new Bundle();
        args.putSerializable(Constants.DIALOG_ARGUMENT, dialog);
        args.putBoolean(Constants.KEYBOARD_ARGUMENT, showKeyboard);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDialog = (Dialog) getArguments().getSerializable(Constants.DIALOG_ARGUMENT);
        mIsShowKeyboard = getArguments().getBoolean(Constants.KEYBOARD_ARGUMENT);

        mPresenter = new MessengerPresenter(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        mPresenter.detachView();
        mMessagesRecyclerAdapter.clear();
        mRespondersAdapter.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_messenger, container, false);

        mRecyclerViewMessages = (RecyclerView) mRootView.findViewById(R.id.recycler_view_messages);
        mRecyclerViewResponders = (RecyclerView) mRootView.findViewById(R.id.recycler_view_responders);

        mEditTextMessage = (EditText) mRootView.findViewById(R.id.edit_text_message);
        mFloatButtonSendMessage = (FloatingActionButton) mRootView.findViewById(R.id.float_button_send_message);

        RecyclerView.LayoutManager messagesLayoutManager = new LinearLayoutManager(
                getContext(), LinearLayoutManager.VERTICAL, true);

        RecyclerView.LayoutManager respondersLayoutManager = new LinearLayoutManager(
                getContext(), LinearLayoutManager.HORIZONTAL, false);

        mRecyclerViewMessages.setLayoutManager(messagesLayoutManager);
        mRecyclerViewMessages.addOnScrollListener(mPaginationScrollListener);
        mRecyclerViewMessages.setHasFixedSize(false);
        mRecyclerViewResponders.setLayoutManager(respondersLayoutManager);

        mFloatButtonSendMessage.setOnClickListener(mOnSendMessageClickListener);

        mMessagesRecyclerAdapter = new MessagesRecyclerAdapter(getContext(), new ArrayList<Message>());
        final StickyRecyclerHeadersDecoration stickyHeaders =
                new StickyRecyclerHeadersDecoration(mMessagesRecyclerAdapter);
        mMessagesRecyclerAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                stickyHeaders.invalidateHeaders();
            }
        });
        mRecyclerViewMessages.addItemDecoration(new StickyRecyclerHeadersDecoration(mMessagesRecyclerAdapter));

        mRecyclerViewMessages.setAdapter(mMessagesRecyclerAdapter);
//        mPresenter.getMessagesInLimit(mDialog, mCurrentMessagesCount, MESSAGES_LOAD_LIMIT);
//        mPresenter.getResponders(mDialog);

        showKeyboard();

        mAllMessagesCount = mPresenter.getMessagesCount(mDialog);
//        mPresenter.getAllMessages(mDialog);
        mPresenter.getMessagesInLimit(mDialog, mCurrentMessagesCount, MESSAGES_LOAD_LIMIT);
        mPresenter.getResponders(mDialog);

        return mRootView;
    }

    @Override
    public void showError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showAllMessages(List<Message> messages) {
        mEditTextMessage.setText("");
        mMessagesRecyclerAdapter.addMessages(messages, false);
        showKeyboard();
    }

    @Override
    public void showNewMessages(List<Message> messages, boolean isPagination) {
        mEditTextMessage.setText("");

        mMessagesRecyclerAdapter.addMessages(messages, isPagination);
        if (isPagination) {
            mRecyclerViewMessages.smoothScrollToPosition(mLastVisibleItem);
            mCurrentMessagesCount += messages.size();
        } else {
            mAllMessagesCount = mPresenter.getMessagesCount(mDialog);
            mCurrentMessagesCount = messages.size();
        }
    }

    @Override
    public void showResponders(List<Responder> responders) {
        mRespondersAdapter = new RespondersRecyclerAdapter(
                getContext(), responders, mOnResponderClickListener
        );
        mRecyclerViewResponders.setAdapter(mRespondersAdapter);
    }

    private void showKeyboard() {
        if (mIsShowKeyboard) {
            mEditTextMessage.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mEditTextMessage.requestFocus();
                    KeyboardUtils.showKeyboard(getContext(), mEditTextMessage);
                }
            }, 50);
        }
    }

}
