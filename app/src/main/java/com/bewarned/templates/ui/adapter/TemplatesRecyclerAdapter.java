package com.bewarned.templates.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bewarned.templates.App;
import com.bewarned.templates.R;
import com.bewarned.templates.data.TemplatesRepository;
import com.bewarned.templates.interfaces.OnStartSpeechListener;
import com.bewarned.templates.model.Template;
import com.bewarned.templates.ui.adapter.holder.TemplateViewHolder;

import java.util.List;

public class TemplatesRecyclerAdapter extends RecyclerView.Adapter<TemplateViewHolder> {

    private Context mContext;
    private List<Template> mTemplatesList;
    private TemplatesRepository mTemplatesRepository;

    public TemplatesRecyclerAdapter(Context context) {
        mContext = context;
        mTemplatesRepository = App.getTemplatesRepository();
        mTemplatesList = mTemplatesRepository.getAll();

        refreshTemplatesList();
    }

    @Override
    public TemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardViewTemplate = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.template_item_layout, parent, false);
        return new TemplateViewHolder(cardViewTemplate);
    }

    @Override
    public void onBindViewHolder(TemplateViewHolder holder, int position) {
        Template currentTemplate = mTemplatesList.get(position);

        holder.getTextViewMessage().setText(currentTemplate.getMessage());
        holder.getToggleButton().setOnCheckedChangeListener(
                new OnStartSpeechListener(currentTemplate.getMessage())
        );
    }

    @Override
    public int getItemCount() {
        return mTemplatesList.size();
    }

    private void refreshTemplatesList() {
        mTemplatesList = mTemplatesRepository.getAll();
        if (mTemplatesList.size() == 0) {
            generateExampleTemplates();
            mTemplatesList = mTemplatesRepository.getAll();
        }
        notifyDataSetChanged();
    }

    void generateExampleTemplates() {
        String[] exampleMessages = mContext.getResources().getStringArray(R.array.arr_example_templates);
        for (String currentMessage : exampleMessages) {
            mTemplatesRepository.create(new Template(currentMessage));
        }
    }
}
