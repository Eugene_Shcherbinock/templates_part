package com.bewarned.templates.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.interfaces.OnShowDialogsListener;
import com.bewarned.templates.interfaces.OnShowEditorListener;
import com.bewarned.templates.interfaces.OnShowMessengerListener;
import com.bewarned.templates.interfaces.OnShowTemplatesListener;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Template;
import com.bewarned.templates.ui.adapter.ViewPagerAdapter;
import com.bewarned.templates.ui.fragment.DialogsFragment;
import com.bewarned.templates.ui.fragment.EditTemplateFragment;
import com.bewarned.templates.ui.fragment.StartFragment;
import com.bewarned.templates.ui.fragment.TemplatesFragment;
import com.bewarned.templates.util.ActivityUtils;
import com.bewarned.templates.util.KeyboardUtils;

public class MainActivity extends AppCompatActivity implements OnShowEditorListener,
        OnShowDialogsListener, OnShowTemplatesListener, OnShowMessengerListener {

    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    private Fragment mCurrentFragment;
    private FrameLayout mLayoutFragmentContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mLayoutFragmentContainer = (FrameLayout) findViewById(R.id.fragment_container);

        if (receiveArguments()) {
            onShowDialogs();
            return;
        }
        initializeViewPager();
    }

    @Override
    @SuppressWarnings("all")
    public void onBackPressed() {
        if (mCurrentFragment instanceof EditTemplateFragment) {
            onShowTemplates();
            mCurrentFragment = null;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onShowEditor(Template template) {
        prepareFragment(EditTemplateFragment.newInstance(template));
    }

    @Override
    @SuppressWarnings("all")
    public void onShowTemplates() {
        initializeViewPager();
        mTabLayout.getTabAt(TemplatesFragment.POSITION).select();
        KeyboardUtils.hideKeyboard(this, mTabLayout);
    }

    @Override
    public void onShowMessenger(Dialog dialog, boolean showKeyboard) {
        Bundle messengerArguments = new Bundle();
        messengerArguments.putLong(Constants.DIALOG_ARGUMENT, dialog.getId());
        messengerArguments.putBoolean(Constants.KEYBOARD_ARGUMENT, showKeyboard);

        ActivityUtils.startActivity(this, MessengerActivity.class, messengerArguments);
    }

    @Override
    @SuppressWarnings("all")
    public void onShowDialogs() {
        initializeViewPager();

        mTabLayout.getTabAt(DialogsFragment.POSITION).select();

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle(getString(R.string.app_name));

        KeyboardUtils.hideKeyboard(this, mTabLayout);
    }

    private void prepareFragment(Fragment fragment) {
        mViewPager.setVisibility(View.GONE);
        mTabLayout.setVisibility(View.GONE);

        if (fragment instanceof EditTemplateFragment) {
            mCurrentFragment = fragment;
            mLayoutFragmentContainer.setVisibility(View.VISIBLE);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    private void initializeViewPager() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addNewFragment(StartFragment.newInstance(), getString(R.string.str_start_fragment_title));
        viewPagerAdapter.addNewFragment(DialogsFragment.newInstance(), getString(R.string.str_dialogs_fragment_title));
        viewPagerAdapter.addNewFragment(TemplatesFragment.newInstance(), getString(R.string.str_templates_fragment_title));

        mViewPager.setAdapter(viewPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        mViewPager.setVisibility(View.VISIBLE);
        mTabLayout.setVisibility(View.VISIBLE);
        mLayoutFragmentContainer.setVisibility(View.GONE);
    }

    private boolean receiveArguments() {
        Intent intentArguments = getIntent();
        return intentArguments.getExtras() != null;
    }
}
