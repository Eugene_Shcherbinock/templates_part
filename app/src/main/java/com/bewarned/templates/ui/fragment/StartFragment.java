package com.bewarned.templates.ui.fragment;


import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.bewarned.templates.App;
import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.contract.StartContract;
import com.bewarned.templates.interfaces.OnShowMessengerListener;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.presenter.StartPresenter;
import com.bewarned.templates.ui.adapter.TemplatesRecyclerAdapter;
import com.nuance.speechkit.Audio;
import com.nuance.speechkit.DetectionType;
import com.nuance.speechkit.PcmFormat;
import com.nuance.speechkit.Recognition;
import com.nuance.speechkit.RecognitionType;
import com.nuance.speechkit.Session;
import com.nuance.speechkit.Transaction;
import com.nuance.speechkit.TransactionException;

import java.util.concurrent.TimeUnit;

public class StartFragment extends Fragment implements StartContract.View {

    private float mCoordinateX;
    private int mCurrentPreviewPosition;

    private View mRootView;

    private TextView mTextViewCounter;
    private Button mButtonStartDictation;
    private Thread mCounterThread = new Thread(new Runnable() {
        @Override
        public void run() {
            for (int i = Constants.MAXIMAL_DICTATION_SECONDS; i >= 0 && App.sRecognizerStarted; i--) {
                final int currentSecond = i;
                mTextViewCounter.post(new Runnable() {
                    @Override
                    public void run() {
                        mTextViewCounter.setText(String.valueOf(currentSecond));
                    }
                });
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            mTextViewCounter.post(new Runnable() {
                @Override
                public void run() {
                    mPresenter.stopDictation();
                }
            });
        }
    });

    private RecyclerView mRecyclerViewTemplates;
    private TemplatesRecyclerAdapter mRecyclerAdapter;

    private FloatingActionButton mFloatButtonCreateDialog;

    private StartContract.Presenter mPresenter;
    private OnShowMessengerListener mOnShowMessengerListener;

    private Session mSpeechSession;

    private Audio mStartEarcon;
    private Audio mStopEarcon;
    private Audio mErrorEarcon;

    private Transaction.Options mTransactionOptions;
    private Transaction mSpeechTransaction;
    private Transaction.Listener mRecognitionListener = new Transaction.Listener() {
        @Override
        public void onRecognition(Transaction transaction, Recognition recognition) {
//            super.onRecognition(transaction, recognition);
            mPresenter.stopDictation();
            mPresenter.createDialogWithMessage(recognition.getText());
        }

        @Override
        public void onError(Transaction transaction, String suggestion, TransactionException e) {
//            super.onError(transaction, suggestion, e);
            mPresenter.stopDictation();
        }
    };

    private View.OnClickListener mDictationStart = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPresenter.startDictation();
            mCounterThread.start();
        }
    };
    private View.OnTouchListener mOnChangeTemplatePreview = new View.OnTouchListener() {
        @Override
        public boolean onTouch(final View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mCoordinateX = event.getX();
                    break;
                case MotionEvent.ACTION_UP:
                    if (mCoordinateX > event.getX()) {
                        if (mCurrentPreviewPosition + 1 < mRecyclerAdapter.getItemCount()) {
                            mCurrentPreviewPosition++;
                        }
                    } else if (mCoordinateX < event.getX()) {
                        if (mCurrentPreviewPosition - 1 >= 0) {
                            mCurrentPreviewPosition--;
                        }
                    }
                    v.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ((RecyclerView) v).smoothScrollToPosition(mCurrentPreviewPosition);
                        }
                    }, 50);

                    break;
            }
            return false;
        }
    };
    private View.OnClickListener mOnCreateDialogClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPresenter.createEmptyDialog();
        }
    };

    public StartFragment() {
        // Required empty public constructor
    }

    public static StartFragment newInstance() {
        return new StartFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShowMessengerListener) {
            mOnShowMessengerListener = (OnShowMessengerListener) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenter = new StartPresenter(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_start, container, false);

        mTextViewCounter = (TextView) mRootView.findViewById(R.id.text_view_counter);
        mButtonStartDictation = (Button) mRootView.findViewById(R.id.button_start_dictation);
        mButtonStartDictation.setOnClickListener(mDictationStart);

        mRecyclerAdapter = new TemplatesRecyclerAdapter(getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(
                getContext(), LinearLayoutManager.HORIZONTAL, false
        );

        mRecyclerViewTemplates = (RecyclerView) mRootView.findViewById(R.id.recycler_view_templates_preview);
        mRecyclerViewTemplates.setLayoutManager(layoutManager);
        mRecyclerViewTemplates.setOnTouchListener(mOnChangeTemplatePreview);
        mRecyclerViewTemplates.setHasFixedSize(false);

        mRecyclerViewTemplates.setAdapter(mRecyclerAdapter);

        mFloatButtonCreateDialog = (FloatingActionButton) mRootView.findViewById(R.id.float_button_start_new_dialog);
        mFloatButtonCreateDialog.setOnClickListener(mOnCreateDialogClickListener);

        loadEarconsSounds();

        setHasOptionsMenu(true);
        return mRootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.start_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showCounter() {
        mButtonStartDictation.setVisibility(View.GONE);
        mTextViewCounter.setVisibility(View.VISIBLE);

        startSpeechInput();
    }

    @Override
    public void hideCounter() {
        mSpeechTransaction.stopRecording();
        mSpeechTransaction.cancel();

        mButtonStartDictation.setVisibility(View.VISIBLE);
        mTextViewCounter.setVisibility(View.GONE);
    }

    @Override
    public void showMessenger(Dialog dialog, boolean showKeyboard) {
        if (mOnShowMessengerListener != null) {
            mOnShowMessengerListener.onShowMessenger(dialog, showKeyboard);
        }
    }

    private void loadEarconsSounds() {
        PcmFormat soundsFormat = new PcmFormat(PcmFormat.SampleFormat.SignedLinear16, 16000, 1);

        mStartEarcon = new Audio(getContext(), R.raw.start_signal, soundsFormat);
        mStopEarcon = new Audio(getContext(), R.raw.stop_signal, soundsFormat);
        mErrorEarcon = new Audio(getContext(), R.raw.error_signal, soundsFormat);
    }

    private void startSpeechInput() {
        mTransactionOptions = new Transaction.Options();
        mTransactionOptions.setDetection(DetectionType.Short);
        mTransactionOptions.setRecognitionType(RecognitionType.DICTATION);
        mTransactionOptions.setEarcons(mStartEarcon, mStopEarcon, mErrorEarcon, mStopEarcon);

        mSpeechSession = Session.Factory.session(
                getContext(), Uri.parse(getString(R.string.nuance_full_url)), getString(R.string.nuance_app_key));

        mSpeechTransaction = mSpeechSession.recognize(mTransactionOptions, mRecognitionListener);
    }
}
