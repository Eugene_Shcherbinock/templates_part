package com.bewarned.templates.ui.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bewarned.templates.App;
import com.bewarned.templates.R;
import com.bewarned.templates.data.DialogsRepository;
import com.bewarned.templates.data.MessagesRepository;
import com.bewarned.templates.data.RespondersRepository;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.model.Responder;
import com.bewarned.templates.ui.adapter.holder.DialogViewHolder;
import com.bewarned.templates.util.DateUtils;

import java.util.Date;
import java.util.List;
import java.util.Random;

public class DialogsListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Dialog> mDialogs;
    private DialogsRepository mDialogsRepository;

    public DialogsListAdapter(Context context, List<Dialog> dialogs) {
        mContext = context;
        mDialogs = dialogs;
        mDialogsRepository = App.getDialogsRepository();

        refreshDialogsList();
    }

    public void deleteDialogs(SparseBooleanArray checkedPlayers) {
        for (int i = (getCount() - 1); i > -1; i--) {
            if (checkedPlayers.get(i)) {
                mDialogsRepository.delete(mDialogs.get(i));
            }
        }
        refreshDialogsList();
    }

    @Override
    public int getCount() {
        return mDialogs.size();
    }

    @Override
    public Dialog getItem(int position) {
        return mDialogs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    @SuppressWarnings("all")
    public View getView(int position, View convertView, ViewGroup parent) {
        DialogViewHolder dialogViewHolder;
        View dialogItemView = convertView;

        if (dialogItemView == null) {
            dialogItemView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.dialog_item_layout, parent, false);

            dialogViewHolder = new DialogViewHolder(dialogItemView);
            dialogItemView.setTag(dialogViewHolder);
        } else {
            dialogViewHolder = (DialogViewHolder) dialogItemView.getTag();
        }

        Dialog currentDialog = mDialogs.get(position);
        String lastMessageTime = DateUtils.getTime(currentDialog.getLastMessageDate());

        dialogViewHolder.getPersonsCountView().setText(currentDialog.getRespondersCount() + "");
        dialogViewHolder.getLastMessageView().setText(currentDialog.getLastMessage());

        if ("".equals(currentDialog.getTitle())) {
            dialogViewHolder.getDialogTitleView().setText(currentDialog.getDefaultTitle());
        } else {
            dialogViewHolder.getDialogTitleView().setText(currentDialog.getTitle());
        }
        dialogViewHolder.getLastMessageTimeView().setText(lastMessageTime);

        return dialogItemView;
    }

    private void refreshDialogsList() {
        mDialogs = mDialogsRepository.getAll();
        if (mDialogs.size() == 0) {
            generateExampleDialogs();
            mDialogs = mDialogsRepository.getAll();
        }
        notifyDataSetChanged();
    }

    void generateExampleDialogs() {
        for (int i = 0; i < 5; i++) {
            Dialog dialog = new Dialog();
            mDialogsRepository.create(dialog);

            List<Responder> dialogResponders = dialog.getResponders();

            int N = new Random().nextInt(4) + 1;
            MessagesRepository messagesRepository = new MessagesRepository(mContext, dialog);
            RespondersRepository respondersRepository = new RespondersRepository(mContext, dialog);
            for (int j = 0; j < N; j++) {
                StringBuilder messageText = new StringBuilder();
                for (int k = 0; k < 4; k++) {
                    messageText.append("My message ");
                }

                Responder responder = dialogResponders.get(j);
                if (j > 0) {
                    responder.setName("User " + j);
                }
                respondersRepository.update(responder.getId(), responder);

                Message message = new Message(responder, messageText.toString(), DateUtils.formatDate(new Date()));
                messagesRepository.create(message);

                dialog.setRespondersCount(dialog.getRespondersCount() + 1);
                dialog.setLastMessage(message.getMessage());
                dialog.setLastMessageDate(message.getDate());
            }
            mDialogsRepository.update(dialog.getId(), dialog);
        }
    }

}
