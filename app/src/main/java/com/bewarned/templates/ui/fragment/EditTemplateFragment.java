package com.bewarned.templates.ui.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.contract.TemplateEditorContract;
import com.bewarned.templates.interfaces.OnShowTemplatesListener;
import com.bewarned.templates.model.Template;
import com.bewarned.templates.presenter.TemplateEditorPresenter;

public class EditTemplateFragment extends Fragment implements TemplateEditorContract.View {

    private Template mTemplate;
    private TemplateEditorContract.Presenter mPresenter;
    private OnShowTemplatesListener mOnShowTemplates;

    private View mRootView;
    private EditText mEditTextTemplate;
    private FloatingActionButton mConfirmButton;

    private View.OnClickListener mConfirmClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String templateMessage = mEditTextTemplate.getText().toString();
            if (mTemplate != null) {
                mTemplate.setMessage(templateMessage);
                mPresenter.editTemplate(mTemplate);
                showTemplates();
                return;
            }
            mPresenter.addTemplate(new Template(templateMessage));
            showTemplates();
        }
    };

    public EditTemplateFragment() {
        // Required empty public constructor
    }

    public static EditTemplateFragment newInstance(Template template) {
        EditTemplateFragment fragment = new EditTemplateFragment();

        if (template != null) {
            Bundle args = new Bundle();
            args.putSerializable(Constants.TEMPLATE_ARGUMENT, template);
            fragment.setArguments(args);
        }

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnShowTemplatesListener) {
            mOnShowTemplates = (OnShowTemplatesListener) context;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTemplate = (Template) getArguments().getSerializable(Constants.TEMPLATE_ARGUMENT);
        }
        mPresenter = new TemplateEditorPresenter(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment_edit_template, container, false);

        mEditTextTemplate = (EditText) mRootView.findViewById(R.id.edit_text_template_message);
        if (mTemplate != null) {
            mEditTextTemplate.setText(mTemplate.getMessage());
        }

        mConfirmButton = (FloatingActionButton) mRootView.findViewById(R.id.button_confirm_template);
        mConfirmButton.setOnClickListener(mConfirmClickListener);

        return mRootView;
    }

    @Override
    public void showError(String message) {
        Snackbar.make(mRootView, message, Snackbar.LENGTH_SHORT).show();
    }

    private void showTemplates() {
        if (mOnShowTemplates != null) {
            mOnShowTemplates.onShowTemplates();
        }
    }
}
