package com.bewarned.templates.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bewarned.templates.R;
import com.bewarned.templates.interfaces.OnStartSpeechListener;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.ui.adapter.holder.MessageDateDividerViewHolder;
import com.bewarned.templates.ui.adapter.holder.MessageViewHolder;
import com.bewarned.templates.ui.adapter.holder.MyMessageViewHolder;
import com.bewarned.templates.util.DateUtils;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;

import java.util.Date;
import java.util.List;

public class MessagesRecyclerAdapter extends RecyclerView.Adapter<MessageViewHolder>
                                     implements StickyRecyclerHeadersAdapter <MessageViewHolder> {

    private final int MY_MESSAGE_ITEM = 0;
    private final int NOT_MY_MESSAGE_ITEM = 1;

    private Context mContext;
    private List<Message> mMessages;

    public MessagesRecyclerAdapter(Context context, List<Message> messages) {
        mContext = context;
        mMessages = messages;
    }

    public Message getItem(int position) {
        return mMessages.get(position);
    }

    public void addMessages(List<Message> newMessages, boolean isPagination) {
        int oldSize = mMessages.size();
        if (isPagination) {
            mMessages.addAll(newMessages);
            notifyItemRangeInserted(oldSize, newMessages.size());
            return;
        }
        mMessages = newMessages;
        notifyDataSetChanged();
    }

    public void clear() {
        mMessages.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        Message currentMessage = mMessages.get(position);
        if (currentMessage.getAuthor().getName().equalsIgnoreCase("me")) {
            return MY_MESSAGE_ITEM;
        }
        return NOT_MY_MESSAGE_ITEM;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View messageItemView;
        if (viewType == MY_MESSAGE_ITEM) {
            messageItemView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.my_message_item_layout, parent, false);
            return new MyMessageViewHolder(messageItemView);
        } else {
            messageItemView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.message_item_layout, parent, false);
            return new MessageViewHolder(messageItemView);
        }
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        Message currentMessage = mMessages.get(position);

        Date currentDate = new Date();
        Date lastMessageDate = new Date(currentMessage.getDate());

        String showingDate = currentMessage.getDate();
        if (DateUtils.compareDates(currentDate, lastMessageDate) == 0) {
            showingDate = "today " + DateUtils.getTime(showingDate);
        }

        holder.getMessageView().setText(currentMessage.getMessage());
        holder.getMessageDateView().setText(showingDate);

        if (holder instanceof MyMessageViewHolder) {
            ((MyMessageViewHolder) holder).getPlayMessageView()
                    .setOnClickListener(new OnStartSpeechListener(currentMessage.getMessage()));
        } else {
            holder.getProfileImageView().setColorFilter(currentMessage.getAuthor().getColor());
        }
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public long getHeaderId(int position) {
//        if (position == 0) {
//            return -1;
//        } else {
            return getItem(position).getDate().charAt(1);
//        }
    }

    @Override
    public MessageViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View headerView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.date_message_divider_layout, parent, false);
        return new MessageDateDividerViewHolder(headerView);
    }

    @Override
    public void onBindHeaderViewHolder(MessageViewHolder holder, int position) {
        ((MessageDateDividerViewHolder) holder)
                .getDateDividerView()
                .setText(DateUtils.getDate(mMessages.get(position).getDate()));
    }
}
