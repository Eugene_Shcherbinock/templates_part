package com.bewarned.templates.ui.adapter.holder;

import android.view.View;
import android.widget.ImageView;

import com.bewarned.templates.R;

public class MyMessageViewHolder extends MessageViewHolder {

    private ImageView mViewPlayMessage;

    public MyMessageViewHolder(View itemView) {
        super(itemView);

        mViewPlayMessage = (ImageView) itemView.findViewById(R.id.image_view_dialog_play_message);
    }

    public ImageView getPlayMessageView() {
        return mViewPlayMessage;
    }
}
