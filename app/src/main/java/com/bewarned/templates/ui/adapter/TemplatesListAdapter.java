package com.bewarned.templates.ui.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bewarned.templates.App;
import com.bewarned.templates.R;
import com.bewarned.templates.data.TemplatesRepository;
import com.bewarned.templates.interfaces.OnStartSpeechListener;
import com.bewarned.templates.model.Template;
import com.bewarned.templates.ui.adapter.holder.TemplateViewHolder;

import java.util.List;

public class TemplatesListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Template> mTemplates;
    private TemplatesRepository mTemplatesRepository;

    public TemplatesListAdapter(Context context, List<Template> templates) {
        mContext = context;
        mTemplates = templates;
        mTemplatesRepository = App.getTemplatesRepository();

        refreshTemplatesList();
    }

    public void deleteTemplates(SparseBooleanArray checkedPlayers) {
        for (int i = (getCount() - 1); i > -1; i--) {
            if (checkedPlayers.get(i)) {
                mTemplatesRepository.delete(mTemplates.get(i));
            }
        }
        refreshTemplatesList();
    }

    @Override
    public int getCount() {
        return mTemplates.size();
    }

    @Override
    public Template getItem(int position) {
        return mTemplates.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        final TemplateViewHolder templateViewHolder;
        View templateItemView = convertView;

        if (templateItemView == null) {
            templateItemView = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.template_item_layout, parent, false);

            templateViewHolder = new TemplateViewHolder(templateItemView);
            templateItemView.setTag(templateViewHolder);
        } else {
            templateViewHolder = (TemplateViewHolder) templateItemView.getTag();
        }

        final Template currentTemplate = mTemplates.get(position);

        templateViewHolder.getTextViewNotification().setVisibility(View.GONE);

        templateViewHolder.getTextViewMessage().setText(currentTemplate.getMessage());
        templateViewHolder.getTextViewMessage().setMaxLines(2);
        templateViewHolder.getTextViewMessage().setEllipsize(null);

        templateViewHolder.getToggleButton().setOnCheckedChangeListener(
                new OnStartSpeechListener(currentTemplate.getMessage()));

        return templateItemView;
    }

    private void refreshTemplatesList() {
        mTemplates = mTemplatesRepository.getAll();
        if (mTemplates.size() == 0) {
            generateExampleTemplates();
            mTemplates = mTemplatesRepository.getAll();
        }
        notifyDataSetChanged();
    }

    void generateExampleTemplates() {
        String[] exampleMessages = mContext.getResources().getStringArray(R.array.arr_example_templates);
        for (String currentMessage : exampleMessages) {
            mTemplatesRepository.create(new Template(currentMessage));
        }
    }
}
