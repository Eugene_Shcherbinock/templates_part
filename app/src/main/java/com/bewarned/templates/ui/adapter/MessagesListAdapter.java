package com.bewarned.templates.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bewarned.templates.R;
import com.bewarned.templates.interfaces.OnStartSpeechListener;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.ui.adapter.holder.MessageViewHolder;

import java.util.Date;
import java.util.List;

public class MessagesListAdapter extends BaseAdapter {

    private Context mContext;
    private List<Message> mMessages;

    public MessagesListAdapter(Context context, List<Message> messages) {
        mContext = context;
        mMessages = messages;
    }

    @Override
    public int getCount() {
        return mMessages.size();
    }

    @Override
    public Message getItem(int position) {
        return mMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    @SuppressWarnings("all")
    public View getView(int position, View convertView, ViewGroup parent) {
        MessageViewHolder messageViewHolder;
        View messageItemView = convertView;
        View playMessageView = null;

        Message currentMessage = mMessages.get(position);
        boolean isMyMessage = currentMessage.getAuthor().getName().equalsIgnoreCase("me");

        if (messageItemView == null) {
            if (!isMyMessage) {
                messageItemView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.message_item_layout, parent, false);

            } else {
                messageItemView = LayoutInflater
                        .from(parent.getContext())
                        .inflate(R.layout.my_message_item_layout, parent, false);
                playMessageView = messageItemView.findViewById(R.id.image_view_dialog_play_message);
            }
            messageViewHolder = new MessageViewHolder(messageItemView);
            messageItemView.setTag(messageViewHolder);
        } else {
            messageViewHolder = (MessageViewHolder) messageItemView.getTag();
        }

        Date currentDate = new Date();
        Date lastMessageDate = new Date(currentMessage.getDate());

        String showingDate = currentMessage.getDate();
        if (new Date(
                currentDate.getYear(),
                currentDate.getMonth() - 1,
                currentDate.getDay())
                .compareTo(new Date(
                        lastMessageDate.getYear(),
                        lastMessageDate.getMonth() - 1,
                        lastMessageDate.getDay())) == 0) {
            showingDate = "today " + showingDate.split(" ")[1];
        }

        messageViewHolder.getMessageView().setText(currentMessage.getMessage());
        messageViewHolder.getMessageDateView().setText(showingDate);

        if (isMyMessage && (playMessageView != null)) {
            playMessageView.setOnClickListener(new OnStartSpeechListener(currentMessage.getMessage()));
        } else {
            messageViewHolder.getProfileImageView().setColorFilter(currentMessage.getAuthor().getColor());
        }

        return messageItemView;
    }
}
