package com.bewarned.templates.ui.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.bewarned.templates.App;
import com.bewarned.templates.R;
import com.bewarned.templates.data.RespondersRepository;
import com.bewarned.templates.interfaces.OnResponderClickListener;
import com.bewarned.templates.interfaces.OnUpdateTitleListener;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Responder;
import com.bewarned.templates.ui.adapter.holder.ResponderViewHolder;
import com.bewarned.templates.util.KeyboardUtils;

import java.util.List;

public class RespondersRecyclerAdapter extends RecyclerView.Adapter<ResponderViewHolder> {

    private Context mContext;
    private List<Responder> mResponders;
    private OnResponderClickListener mOnResponderClickListener;

    private int mSelectedPosition;
    private EditText mEditTextResponderName;
    private AlertDialog mAlertDialog;

    private OnUpdateTitleListener mOnUpdateTitleListener;

    private DialogInterface.OnClickListener mOnDialogButtonClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case DialogInterface.BUTTON_POSITIVE:
                    if (!mEditTextResponderName.getText().toString().isEmpty()) {
                        Responder selectedResponder = mResponders.get(mSelectedPosition);
                        selectedResponder.setName(mEditTextResponderName.getText().toString());
                        updateResponder(selectedResponder);
                    }
                    break;
                default:
                    KeyboardUtils.hideKeyboard(mContext, mEditTextResponderName);
                    dialog.dismiss();
            }
        }
    };

    public RespondersRecyclerAdapter(Context context, List<Responder> responders, OnResponderClickListener listener) {
        mContext = context;
        mResponders = responders;
        mOnResponderClickListener = listener;

        if (mContext instanceof OnUpdateTitleListener) {
            mOnUpdateTitleListener = (OnUpdateTitleListener) mContext;
        }

        mSelectedPosition = 0;

        mEditTextResponderName = new EditText(mContext);
        mEditTextResponderName.setGravity(Gravity.CENTER);
        mEditTextResponderName.setHint(R.string.str_input_responder_name_hint);

        mAlertDialog = new AlertDialog.Builder(mContext)
                .setTitle(R.string.str_add_responder_dialog_title)
                .setMessage(R.string.str_add_responder_dialog_message)
                .setCancelable(false)
                .setView(mEditTextResponderName)
                .setPositiveButton(
                        R.string.str_add_responder_dialog_positive_button,
                        mOnDialogButtonClickListener)
                .setNegativeButton(
                        R.string.str_add_responder_dialog_negative_button,
                        mOnDialogButtonClickListener)
                .create();
    }

    public void selectItem(Responder responder) {
        mSelectedPosition = mResponders.indexOf(responder);
        if (responder.getName().isEmpty()) {
            addNewResponder();
        } else {
            notifyDataSetChanged();
        }
    }

    public Responder getSelectedResponder() {
        return mResponders.get(mSelectedPosition);
    }

    public void clear() {
        mResponders.clear();
        notifyDataSetChanged();
    }

    @Override
    public ResponderViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View responderItemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.responder_item_layout, parent, false);
        return new ResponderViewHolder(responderItemView);
    }

    @Override
    public void onBindViewHolder(ResponderViewHolder holder, int position) {
        Responder currentResponder = mResponders.get(position);

        if (mSelectedPosition == position) {
            holder.getActiveIndicatorView().setVisibility(View.VISIBLE);
        } else {
            holder.getActiveIndicatorView().setVisibility(View.GONE);
        }

        boolean isMyMessage = currentResponder.getName().equalsIgnoreCase("me");
        if (isMyMessage) {
            holder.getResponderIconView().setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_profile_image));
            holder.getResponderIconView().setColorFilter(mContext.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.getResponderIconView().setColorFilter(currentResponder.getColor());
        }

        if ("".equals(currentResponder.getName())) {
            holder.getResponderNameView().setVisibility(View.GONE);
        } else {
            holder.getResponderNameView().setVisibility(View.VISIBLE);
            holder.getResponderNameView().setText(currentResponder.getName().replace(" ", "\n"));
        }
        holder.setItemClickListener(currentResponder, mOnResponderClickListener);
    }

    @Override
    public int getItemCount() {
        return mResponders.size();
    }

    private void addNewResponder() {
        mAlertDialog.show();
        mEditTextResponderName.postDelayed(new Runnable() {
            @Override
            public void run() {
                mEditTextResponderName.requestFocus();
                KeyboardUtils.showKeyboard(mContext, mEditTextResponderName);
            }
        }, 50);
    }

    private void updateResponder(Responder responder) {
        RespondersRepository respondersRepository = new RespondersRepository(mContext, responder.getDialog());
        respondersRepository.update(responder.getId(), responder);

        Dialog responderDialog = responder.getDialog();
        responderDialog.setRespondersCount(responderDialog.getRespondersCount() + 1);

        if (mOnUpdateTitleListener != null) {
            mOnUpdateTitleListener.onUpdateDefaultTitle(responderDialog.getRespondersCount());
        }

        App.getDialogsRepository().update(responderDialog.getId(), responderDialog);
        notifyDataSetChanged();
    }

}
