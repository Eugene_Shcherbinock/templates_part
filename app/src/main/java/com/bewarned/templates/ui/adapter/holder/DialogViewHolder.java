package com.bewarned.templates.ui.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bewarned.templates.R;

public class DialogViewHolder extends RecyclerView.ViewHolder {

    private TextView mTextViewPersonsCount;
    private TextView mTextViewDialogTitle;
    private TextView mTextViewLastMessage;
    private TextView mTextViewLastMessageTime;

    public DialogViewHolder(View itemView) {
        super(itemView);

        mTextViewPersonsCount = (TextView) itemView.findViewById(R.id.text_view_persons_count);
        mTextViewDialogTitle = (TextView) itemView.findViewById(R.id.text_view_dialog_title);
        mTextViewLastMessage = (TextView) itemView.findViewById(R.id.text_view_last_message);
        mTextViewLastMessageTime = (TextView) itemView.findViewById(R.id.text_view_last_message_time);
    }

    public TextView getPersonsCountView() {
        return mTextViewPersonsCount;
    }

    public TextView getDialogTitleView() {
        return mTextViewDialogTitle;
    }

    public TextView getLastMessageView() {
        return mTextViewLastMessage;
    }

    public TextView getLastMessageTimeView() {
        return mTextViewLastMessageTime;
    }
}
