package com.bewarned.templates;

import android.provider.BaseColumns;

public class Constants {

    public static final String TEMPLATE_ARGUMENT = "template";

    public static final String VIEW_PAGER_POSITION_ARGUMENT = "messenger_argument";

    public static final int MESSAGES_LOAD_LIMIT = 5;

    public static final int MAXIMAL_DICTATION_SECONDS = 10;

    public static final String DIALOG_ARGUMENT = "dialog";

    public static final String KEYBOARD_ARGUMENT = "show_keyboard";

    public class TemplatesTable implements BaseColumns {

        public static final String TABLE = "templates";

        public static final String ID = BaseColumns._ID;

        public static final String MESSAGE = "message";

    }

    public class DialogsTable implements BaseColumns {

        public static final String TABLE = "dialogs";

        public static final String ID = BaseColumns._ID;

        public static final String TITLE = "title";

        public static final String DEFAULT_TITLE = "default_title";

        public static final String RESPONDERS_COUNT = "responders_count";

        public static final String LAST_MESSAGE_DATE = "last_message_date";

        public static final String LAST_MESSAGE = "last_message";

    }

    public class MessagesTable implements BaseColumns {

        public static final String DIALOG = "dialog";

        public static final String TABLE = "messages";

        public static final String ID = BaseColumns._ID;

        public static final String RESPONDER = "responder";

        public static final String MESSAGE = "message";

        public static final String DATE = "date";

    }

    public class RespondersTable implements BaseColumns {

        public static final String DIALOG = "dialog";

        public static final String TABLE = "responders";

        public static final String ID = BaseColumns._ID;

        public static final String NAME = "name";

        public static final String COLOR = "color";

    }

}
