package com.bewarned.templates;

import android.app.Application;
import android.content.Context;
import android.speech.tts.TextToSpeech;

import com.activeandroid.ActiveAndroid;
import com.bewarned.templates.data.DialogsRepository;
import com.bewarned.templates.data.TemplatesRepository;

import java.util.Locale;

public class App extends Application {

    private static Context sContext;

    public static boolean sRecognizerStarted;
    public static boolean sTextToSpeechWorking;

    private static TextToSpeech sTextToSpeechService;
    private static TemplatesRepository sTemplatesRepository;
    private static DialogsRepository sDialogsRepository;

    private static TextToSpeech.OnInitListener sTextToSpeechInit = new TextToSpeech.OnInitListener() {
        @Override
        public void onInit(int status) {
            if (status != TextToSpeech.ERROR) {
                sTextToSpeechService.setLanguage(Locale.ENGLISH);
            }
        }
    };

    public static Context getContext() {
        return sContext;
    }

    public static TemplatesRepository getTemplatesRepository() {
        if (sTemplatesRepository == null) {
            sTemplatesRepository = new TemplatesRepository(sContext);
        }
        return sTemplatesRepository;
    }

    public static DialogsRepository getDialogsRepository() {
        if (sDialogsRepository == null) {
            sDialogsRepository = new DialogsRepository(sContext);
        }
        return sDialogsRepository;
    }

    public static TextToSpeech getTextToSpeechService() {
        if (sTextToSpeechService == null) {
            sTextToSpeechService = new TextToSpeech(sContext, sTextToSpeechInit);
        }
        return sTextToSpeechService;
    }

//    public synchronized static boolean isTextToSpeechWorking() {
//        return sTextToSpeechWorking;
//    }
//
//    public synchronized static void textToSpeechWorking(boolean isWorking) {
//        sTextToSpeechWorking = isWorking;
//    }

    @Override
    public void onCreate() {
        super.onCreate();

        sTextToSpeechWorking = false;
        sRecognizerStarted = false;

        sContext = getApplicationContext();
        ActiveAndroid.initialize(sContext);
    }

}
