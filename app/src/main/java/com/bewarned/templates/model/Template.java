package com.bewarned.templates.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.bewarned.templates.Constants;

import java.io.Serializable;

@Table(name = Constants.TemplatesTable.TABLE, id = Constants.TemplatesTable.ID)
public class Template extends Model implements Serializable {

    @Column(name = Constants.TemplatesTable.MESSAGE, notNull = true)
    private String mMessageText;

    public Template() {
        super();
    }

    public Template(String message) {
        super();
        mMessageText = message;
    }

    public String getMessage() {
        return mMessageText;
    }

    public void setMessage(String message) {
        mMessageText = message;
    }

}
