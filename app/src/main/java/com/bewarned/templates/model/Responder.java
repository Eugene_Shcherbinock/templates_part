package com.bewarned.templates.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.bewarned.templates.Constants;

import java.io.Serializable;

@Table(name = Constants.RespondersTable.TABLE, id = Constants.RespondersTable.ID)
public class Responder extends Model implements Serializable {

    @Column(name = Constants.RespondersTable.NAME)
    private String mName;

    @Column(name = Constants.RespondersTable.COLOR)
    private int mColor;

    @Column(name = Constants.RespondersTable.DIALOG,
            onDelete = Column.ForeignKeyAction.CASCADE,
            notNull = true)
    private Dialog mDialog;

    public Responder() {
        super();
    }

    public Responder(String name, int color) {
        mName = name;
        mColor = color;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int color) {
        mColor = color;
    }

    public Dialog getDialog() {
        return mDialog;
    }

    public void setDialog(Dialog dialog) {
        mDialog = dialog;
    }

}
