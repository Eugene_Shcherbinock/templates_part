package com.bewarned.templates.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.bewarned.templates.Constants;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

@Table(name = Constants.MessagesTable.TABLE, id = Constants.MessagesTable.ID)
public class Message extends Model implements Serializable {

    @Column(name = Constants.MessagesTable.DATE, notNull = true)
    private String mDate;
    @Column(name = Constants.MessagesTable.RESPONDER, notNull = true)
    private Responder mResponder;
    @Column(name = Constants.MessagesTable.MESSAGE, notNull = true)
    private String mMessage;
    @Column(name = Constants.MessagesTable.DIALOG,
            onDelete = Column.ForeignKeyAction.CASCADE,
            notNull = true)
    private Dialog mDialog;

    public Message() {
        super();
    }

    public Message(Responder author, String message, String date) {
        super();
        mResponder = author;
        mMessage = message;
        mDate = date;
    }

    public Dialog getDialog() {
        return mDialog;
    }

    public void setDialog(Dialog dialog) {
        mDialog = dialog;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public Responder getAuthor() {
        return mResponder;
    }

    public void setAuthor(Responder author) {
        mResponder = author;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

    public static class MessageByDateComparator implements Comparator<Message> {
        @Override
        public int compare(Message lhs, Message rhs) {
            return new Date(rhs.getDate()).compareTo(new Date(lhs.getDate()));
        }
    }

}
