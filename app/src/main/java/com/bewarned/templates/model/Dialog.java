package com.bewarned.templates.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.bewarned.templates.Constants;
import com.bewarned.templates.util.DateUtils;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Table(name = Constants.DialogsTable.TABLE, id = Constants.DialogsTable.ID)
public class Dialog extends Model implements Serializable {

    @Column(name = Constants.DialogsTable.RESPONDERS_COUNT)
    private int mRespondersCount;

    @Column(name = Constants.DialogsTable.TITLE)
    private String mTitle;

    @Column(name = Constants.DialogsTable.DEFAULT_TITLE)
    private String mDefaultTitle;

    @Column(name = Constants.DialogsTable.LAST_MESSAGE)
    private String mLastMessage;

    @Column(name = Constants.DialogsTable.LAST_MESSAGE_DATE)
    private String mLastMessageDate;

    public Dialog() {
        super();

        mTitle = "";
        mLastMessage = "";
        mLastMessageDate = "";
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDefaultTitle() {
        return mDefaultTitle;
    }

    public int getRespondersCount() {
        return mRespondersCount;
    }

    public void setRespondersCount(int count) {
        mRespondersCount = count;
        updateDefaultTitle();
    }

    public String getLastMessageDate() {
        return mLastMessageDate;
    }

    public void setLastMessageDate(String date) {
        mLastMessageDate = date;
        updateDefaultTitle();
    }

    public String getLastMessage() {
        return mLastMessage;
    }

    public void setLastMessage(String lastMessage) {
        mLastMessage = lastMessage;
        updateDefaultTitle();
    }

    public List<Message> getMessages() {
        List<Message> messages = getMany(Message.class, Constants.MessagesTable.DIALOG);
        Collections.sort(messages, new Message.MessageByDateComparator());
        return messages;
    }

    public List<Responder> getResponders() {
        return getMany(Responder.class, Constants.RespondersTable.DIALOG);
    }

    private void updateDefaultTitle() {
        mDefaultTitle = mRespondersCount + "-persons dialog, " + DateUtils.getDate(mLastMessageDate);
    }

}
