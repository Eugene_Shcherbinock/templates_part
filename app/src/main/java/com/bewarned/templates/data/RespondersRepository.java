package com.bewarned.templates.data;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.model.Responder;

import java.util.ArrayList;
import java.util.List;

public class RespondersRepository implements Repository<Responder> {

    private Context mContext;
    private Dialog mDialog;

    public RespondersRepository(Context context, Dialog dialog) {
        mContext = context;
        mDialog = dialog;
    }

    @Override
    public boolean create(Responder model) {
        ActiveAndroid.beginTransaction();
        try {
            model.setDialog(mDialog);
            model.save();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_create_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public boolean update(long id, Responder model) {
        ActiveAndroid.beginTransaction();
        try {
            new Update(Responder.class)
                    .set(
                            Constants.RespondersTable.NAME + " = ?",
                            model.getName())
                    .where(Constants.RespondersTable.ID + " = ?", id)
                    .execute();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(
                    mContext,
                    mContext.getString(R.string.str_update_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public boolean delete(Responder model) {
        return false;
    }

    @Override
    public Responder getById(long id) {
        Responder resultResponder = null;

        ActiveAndroid.beginTransaction();
        try {
            resultResponder = Dialog.load(Responder.class, id);
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return resultResponder;
    }

    @Override
    public List<Responder> getAll() {
        List<Responder> responders = null;

        ActiveAndroid.beginTransaction();
        try {
            responders = new Select()
                    .from(Message.class)
                    .where(Constants.RespondersTable.DIALOG + " = ?", mDialog.getId())
                    .execute();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            responders = new ArrayList<>();
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return responders;
    }
}
