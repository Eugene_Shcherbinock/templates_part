package com.bewarned.templates.data;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.model.Template;

import java.util.ArrayList;
import java.util.List;

public class TemplatesRepository implements Repository<Template> {

    private Context mContext;

    public TemplatesRepository(Context context) {
        mContext = context;
    }

    @Override
    public boolean create(Template model) {
        ActiveAndroid.beginTransaction();
        try {
            model.save();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_create_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public boolean update(long id, Template model) {
        ActiveAndroid.beginTransaction();
        try {
            new Update(Template.class)
                    .set(Constants.TemplatesTable.MESSAGE + " = ?", model.getMessage())
                    .where(Constants.TemplatesTable.ID + " = ?", id)
                    .execute();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(
                    mContext,
                    mContext.getString(R.string.str_update_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    public boolean delete(Template model) {
        ActiveAndroid.beginTransaction();
        try {
            model.delete();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_delete_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public Template getById(long id) {
        Template resultTemplate = null;

        ActiveAndroid.beginTransaction();
        try {
            resultTemplate = Template.load(Template.class, id);
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return resultTemplate;
    }

    @Override
    public List<Template> getAll() {
        List<Template> templates = null;

        ActiveAndroid.beginTransaction();
        try {
            templates = new Select()
                    .all()
                    .from(Template.class)
                    .execute();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            templates = new ArrayList<>();
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return templates;
    }

}
