package com.bewarned.templates.data;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;

import java.util.ArrayList;
import java.util.List;

public class MessagesRepository implements Repository<Message> {

    private Context mContext;
    private Dialog mDialog;

    public MessagesRepository(Context context, Dialog dialog) {
        mContext = context;
        mDialog = dialog;
    }

    @Override
    public boolean create(Message message) {
        ActiveAndroid.beginTransaction();
        try {
            message.setDialog(mDialog);
            message.save();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_create_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public boolean delete(Message message) {
        ActiveAndroid.beginTransaction();
        try {
            message.delete();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_delete_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public boolean update(long id, Message model) {
        return false;
    }

    @Override
    public Message getById(long id) {
        return null;
    }

    public int getCount() {
        int result = 0;
        ActiveAndroid.beginTransaction();
        try {
            result = new Select()
                    .from(Message.class)
                    .where(Constants.MessagesTable.DIALOG + " = ?", mDialog.getId())
                    .count();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return result;
    }

    public List<Message> getInLimits(int offset, int count) {
        List<Message> messages = null;

        ActiveAndroid.beginTransaction();
        try {
            messages = new Select()
                    .from(Message.class)
                    .where(Constants.MessagesTable.DIALOG + " = ?", mDialog.getId())
                    .offset(offset)
                    .limit(count)
                    .orderBy(Constants.MessagesTable.ID + " DESC")
                    .execute();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            messages = new ArrayList<>();
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return messages;
    }

    @Override
    public List<Message> getAll() {
        List<Message> messages = null;

        ActiveAndroid.beginTransaction();
        try {
            messages = new Select()
                    .from(Message.class)
                    .where(Constants.MessagesTable.DIALOG + " = ?", mDialog.getId())
                    .orderBy(Constants.MessagesTable.ID + " DESC")
                    .execute();

            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            messages = new ArrayList<>();
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return messages;
    }

}
