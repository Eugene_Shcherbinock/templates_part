package com.bewarned.templates.data;

import com.activeandroid.Model;

import java.util.List;

public interface Repository<T extends Model> {

    boolean create(T model);

    boolean update(long id, T model);

    boolean delete(T model);

    T getById(long id);

    List<T> getAll();

}
