package com.bewarned.templates.data;

import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.activeandroid.query.Update;
import com.bewarned.templates.Constants;
import com.bewarned.templates.R;
import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.model.Responder;

import java.util.ArrayList;
import java.util.List;

public class DialogsRepository implements Repository<Dialog> {

    private Context mContext;

    public DialogsRepository(Context context) {
        mContext = context;
    }

    @Override
    public boolean create(Dialog model) {
        ActiveAndroid.beginTransaction();
        try {
            model.save();
            generateResponders(model);
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_create_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public boolean update(long id, Dialog model) {
        ActiveAndroid.beginTransaction();
        try {
            new Update(Dialog.class)
                    .set(
                            Constants.DialogsTable.TITLE + " = ?, " +
                                    Constants.DialogsTable.DEFAULT_TITLE + " = ?, " +
                                    Constants.DialogsTable.RESPONDERS_COUNT + " = ?, " +
                                    Constants.DialogsTable.LAST_MESSAGE + " = ?, " +
                                    Constants.DialogsTable.LAST_MESSAGE_DATE + " = ?",
                            model.getTitle(), model.getDefaultTitle(), model.getRespondersCount(),
                            model.getLastMessage(), model.getLastMessageDate())
                    .where(Constants.DialogsTable.ID + " = ?", id)
                    .execute();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(
                    mContext,
                    mContext.getString(R.string.str_update_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public boolean delete(Dialog model) {
        ActiveAndroid.beginTransaction();
        try {
            new Delete().from(Message.class).where(Constants.MessagesTable.DIALOG + " = ?", model).execute();
            new Delete().from(Responder.class).where(Constants.RespondersTable.DIALOG + " = ?", model).execute();
            model.delete();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_delete_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }
        return true;
    }

    @Override
    public Dialog getById(long id) {
        Dialog resultDialog = null;

        ActiveAndroid.beginTransaction();
        try {
            resultDialog = Dialog.load(Dialog.class, id);
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return resultDialog;
    }

    @Override
    public List<Dialog> getAll() {
        List<Dialog> dialogs = null;

        ActiveAndroid.beginTransaction();
        try {
            dialogs = new Select()
                    .all()
                    .from(Dialog.class)
                    .orderBy(Constants.DialogsTable.LAST_MESSAGE_DATE + " DESC")
                    .execute();
            ActiveAndroid.setTransactionSuccessful();
        } catch (SQLiteException exception) {
            dialogs = new ArrayList<>();
            Toast.makeText(mContext,
                    mContext.getString(R.string.str_read_sql_exception),
                    Toast.LENGTH_LONG
            ).show();
        } finally {
            ActiveAndroid.endTransaction();
        }

        return dialogs;
    }

    private void generateResponders(Dialog dialog) {
        RespondersRepository respondersRepository = new RespondersRepository(mContext, dialog);
        respondersRepository.create(new Responder("Me", 0));
        respondersRepository.create(new Responder("", mContext.getResources().getColor(R.color.colorResponderFirst)));
        respondersRepository.create(new Responder("", mContext.getResources().getColor(R.color.colorResponderSecond)));
        respondersRepository.create(new Responder("", mContext.getResources().getColor(R.color.colorResponderThird)));
    }
}
