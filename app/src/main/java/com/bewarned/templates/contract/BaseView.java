package com.bewarned.templates.contract;

import android.content.Context;

public interface BaseView {

    Context getContext();

}
