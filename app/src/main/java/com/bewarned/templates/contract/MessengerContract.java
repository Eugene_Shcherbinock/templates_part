package com.bewarned.templates.contract;

import com.bewarned.templates.model.Dialog;
import com.bewarned.templates.model.Message;
import com.bewarned.templates.model.Responder;

import java.util.List;

public interface MessengerContract {

    interface View extends BaseView {

        void showAllMessages(List<Message> messages);

        void showNewMessages(List<Message> messages, boolean isPagination);

        void showResponders(List<Responder> responders);

        void showError(String error);

    }

    interface Presenter extends BasePresenter<View> {

        void getAllMessages(Dialog dialog);

        int getMessagesCount(Dialog dialog);

        void getMessagesInLimit(Dialog dialog, int offset, int count);

        void getResponders(Dialog dialog);

        void sendMessage(Dialog dialog, Message message);

    }

}
