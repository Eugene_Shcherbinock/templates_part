package com.bewarned.templates.contract;

public interface BasePresenter<V extends BaseView> {

    void attachView(V view);

    void detachView();

}
