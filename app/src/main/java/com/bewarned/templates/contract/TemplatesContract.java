package com.bewarned.templates.contract;

import com.bewarned.templates.model.Template;

import java.util.List;

public interface TemplatesContract {

    interface View extends BaseView {

        void showTemplates(List<Template> templates);

        void editTemplate(Template template);

    }

    interface Presenter extends BasePresenter<View> {

        void getTemplatesList();

        void selectTemplate(Template template);

        void addTemplate();

    }

}
