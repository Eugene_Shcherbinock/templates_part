package com.bewarned.templates.contract;

import com.bewarned.templates.model.Template;

public interface TemplateEditorContract {

    interface View extends BaseView {

        void showError(String message);

    }

    interface Presenter extends BasePresenter<View> {

        void editTemplate(Template template);

        void addTemplate(Template template);

    }

}
