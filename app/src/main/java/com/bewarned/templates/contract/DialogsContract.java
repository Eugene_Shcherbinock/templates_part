package com.bewarned.templates.contract;

import com.bewarned.templates.model.Dialog;

import java.util.List;

public interface DialogsContract {

    interface View extends BaseView {

        void showDialogs(List<Dialog> dialogs);

        void showDialog(Dialog dialog);

    }

    interface Presenter extends BasePresenter<View> {

        void getDialogs();

        void selectDialog(Dialog dialog);

        void createDialog();

    }

}
