package com.bewarned.templates.contract;

import com.bewarned.templates.model.Dialog;

public interface StartContract {

    interface View extends BaseView {

        void showCounter();

        void hideCounter();

        void showMessenger(Dialog dialog, boolean showKeyboard);

    }

    interface Presenter extends BasePresenter<View> {

        void startDictation();

        void stopDictation();

        void createEmptyDialog();

        void createDialogWithMessage(String message);

    }

}
