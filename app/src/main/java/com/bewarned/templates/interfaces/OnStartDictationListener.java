package com.bewarned.templates.interfaces;

import android.support.annotation.NonNull;

public interface OnStartDictationListener {

    void onDictationStart(@NonNull OnStopDictationListener listener);

}
