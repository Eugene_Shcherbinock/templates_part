package com.bewarned.templates.interfaces;

import com.bewarned.templates.model.Responder;

public interface OnResponderClickListener {

    void onResponderClick(Responder responder);

}
