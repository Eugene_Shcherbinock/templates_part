package com.bewarned.templates.interfaces;

public interface OnUpdateTitleListener {

    void onUpdateDefaultTitle(int respondersCount);

}
