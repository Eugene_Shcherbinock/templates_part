package com.bewarned.templates.interfaces;

public interface OnShowTemplatesListener {

    void onShowTemplates();

}
