package com.bewarned.templates.interfaces;

import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import com.bewarned.templates.App;

import java.util.HashMap;

public class OnStartSpeechListener implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private String mMessage;
    private TextToSpeech mTextToSpeech;
    private HashMap<String, String> mSpeechParams;

    public OnStartSpeechListener(String message) {
        mMessage = message;
        mSpeechParams = new HashMap<>();
        mTextToSpeech = App.getTextToSpeechService();
    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, boolean isChecked) {
        Log.d("SpeechService", App.sTextToSpeechWorking + " - start");
        if (!App.sTextToSpeechWorking) {
            if (isChecked) {
                App.sTextToSpeechWorking = true;
                if (!mTextToSpeech.isSpeaking()) {
                    mSpeechParams.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "stringId");
                    mTextToSpeech.speak(mMessage, TextToSpeech.QUEUE_FLUSH, mSpeechParams);
                    mTextToSpeech.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
                        @Override
                        public void onUtteranceCompleted(String utteranceId) {
                            buttonView.post(new Runnable() {
                                @Override
                                public void run() {
                                    buttonView.setChecked(false);
                                    mTextToSpeech.stop();
                                    App.sTextToSpeechWorking = false;
                                }
                            });
                        }
                    });
                    return;
                }
            }
        } else {
            buttonView.post(new Runnable() {
                @Override
                public void run() {
                    buttonView.setChecked(false);
                }
            });
        }

        if (mTextToSpeech.isSpeaking()) {
            App.sTextToSpeechWorking = false;
            mTextToSpeech.stop();
        }
        Log.d("SpeechService", App.sTextToSpeechWorking + " - end");
    }

    @Override
    public void onClick(View v) {
        if (!App.sTextToSpeechWorking) {
            if (!mTextToSpeech.isSpeaking()) {
                mSpeechParams.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "stringId");
                mTextToSpeech.speak(mMessage, TextToSpeech.QUEUE_FLUSH, mSpeechParams);
            }
        }
    }
}
