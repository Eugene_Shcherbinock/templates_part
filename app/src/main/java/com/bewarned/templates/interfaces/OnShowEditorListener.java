package com.bewarned.templates.interfaces;

import com.bewarned.templates.model.Template;

public interface OnShowEditorListener {

    void onShowEditor(Template template);

}
