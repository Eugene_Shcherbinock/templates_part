package com.bewarned.templates.interfaces;

import java.util.List;

public interface OnStopDictationListener {

    void onDictationStop(String result);

}
