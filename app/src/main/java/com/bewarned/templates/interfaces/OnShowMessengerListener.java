package com.bewarned.templates.interfaces;

import com.bewarned.templates.model.Dialog;

public interface OnShowMessengerListener {

    void onShowMessenger(Dialog dialog, boolean showKeyboard);

}
