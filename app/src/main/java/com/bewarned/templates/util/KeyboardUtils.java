package com.bewarned.templates.util;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtils {

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager keyboardManager =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboardManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void showKeyboard(Context context, View view) {
        InputMethodManager keyboardManager =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboardManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

}
