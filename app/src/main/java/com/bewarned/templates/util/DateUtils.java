package com.bewarned.templates.util;

import com.bewarned.templates.App;
import com.bewarned.templates.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static String formatDate(Date date) {
        return new SimpleDateFormat(App.getContext().getString(R.string.str_date_format), Locale.US).format(date);
    }

    public static int compareDates(Date currentDate, Date lastMessageDate) {
        return new Date(
                currentDate.getYear(),
                currentDate.getMonth() - 1,
                currentDate.getDay())
                .compareTo(new Date(
                        lastMessageDate.getYear(),
                        lastMessageDate.getMonth() - 1,
                        lastMessageDate.getDay()));
    }

    public static String getDate(String date) {
        return date.split(" ")[0];
    }

    public static String getTime(String date) {
        return date.split(" ")[1];
    }

}
